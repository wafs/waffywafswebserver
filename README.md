# WaffyWafs Webserver #

This is a project created for the sole purpose of a learning experience on "How does the internet even internet?".

I'm trying not to read too much stuff online on how things work, unless I'm completely stuck. For the most part my intuition seems ok.
The project is still in a very early stage, for example only get requests work at the moment and you can't really inject any data dynamically, but that will come in time.

Entry point to the program is at 


```
#!java

/src/Server/Main.java
```