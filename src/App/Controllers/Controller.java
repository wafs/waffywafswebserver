package App.Controllers;

import HTTP.Request;
import Server.ServerConstants;

import java.io.*;

/**
 * Created by wafs on 23/09/15.
 */
public abstract class Controller {

    protected Request request;
    protected final String viewPath = ServerConstants.getPath("views") + "/";
    protected File template;

    public Controller(Request request){
        this.request = request;
    }

    public File getTemplate() {
        return template;

    }

    public void setTemplate(String path) throws FileNotFoundException {
        File f = new File(viewPath + path);
        if (!f.isFile()) {
            String s = String.format("The file at '%s' could not be found", path);
            throw new FileNotFoundException(s);
        }
        this.template = f;
    }

    public String fetchTemplate() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(template));
        StringBuilder sb = new StringBuilder();
        String next;
        while ((next = br.readLine()) != null) {
            sb.append(next);
        }
        return sb.toString();
    }
}