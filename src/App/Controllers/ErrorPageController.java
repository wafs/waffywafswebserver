package App.Controllers;

import HTTP.BadResponse;
import HTTP.Request;

/**
 * Created by wafs on 24/09/15.
 */
public class ErrorPageController extends Controller{
    public ErrorPageController(Request request){
        super(request);
    }
    public BadResponse index(){
        try{
            setTemplate("404.html");
            String fetch = fetchTemplate();
            return new BadResponse(fetch);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
