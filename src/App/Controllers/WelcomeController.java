package App.Controllers;

import HTTP.Request;
import HTTP.Response;

/**
 * Created by wafs on 23/09/15.
 */
public class WelcomeController  extends Controller{

    public WelcomeController(Request request){
        super(request);
    }
    public Response index(){
        try{
            setTemplate("index.html");
            String fetch = fetchTemplate();
            return new Response(fetch);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
