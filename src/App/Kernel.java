package App;

import HTTP.AbstractResponse;
import HTTP.BadResponse;
import HTTP.Request;
import HTTP.Response;
import Router.Router;
import Server.ServerConstants;

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Thread to handle connections to the web server
 */
public class Kernel implements Runnable {
    private Socket socket;
    private PrintWriter pw;
    private Router router;

    public Kernel(Socket socket){
        this.socket = socket;
        try {
            router = Bootstrap.getRouter();
        }catch (FileNotFoundException f){
            System.err.println("Route configuration not found at required path");
        }
    }

    /**
     * Attempts to read request, process said request, collect response data and then sends it.
     */
    @Override
    public void run() {
        try{
            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            pw = new PrintWriter(socket.getOutputStream(),true);
            StringBuilder sb = new StringBuilder();
            AbstractResponse response;
            String rawRequest;
            String nextLine;

            // Collect raw request
            while((nextLine = br.readLine()) != null && br.ready()){
                sb.append(nextLine);
                sb.append("\n");
            }
            rawRequest = sb.toString();
            Request request = new Request(rawRequest);


            // Dump
            System.out.println(request.getHeader());
            System.out.println(request.getBody());


            // Build appropriate response
            String requestPath = request.getPath();
            // If it's a file, return the file
            File f = new File(ServerConstants.getPath("web") + requestPath);
            if(f.isFile()){
                // TODO : Load a bunch of mimetypes at the start when server is loaded
                String mimeType = "";
                if(requestPath.endsWith("css")){
                    mimeType = "text/css";
                }else if(requestPath.endsWith("js")){
                    mimeType = "text/javascript";
                }

                response = new Response(new String(Files.readAllBytes(Paths.get(f.toString()))));
                response.setContentType(mimeType);
            }else{
                response = router.invokeRoute(requestPath, request);
            }
            // Return response
            response.sendResponse(pw);

            socket.close();
        }catch(FileNotFoundException f){
            new BadResponse(String.format("<h1>404 : File Not found\n\n<pre>%s</pre>", f.getMessage())).sendResponse(pw);
        }catch(Exception e){
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String stackTrace = sw.toString();
            new BadResponse(String.format("<h1>404 Resource Not Found</h1>\n<pre>%s</pre>", stackTrace)).sendResponse(pw);
        }
    }
}