package App;

import Router.Router;
import Server.ServerConstants;

import java.io.FileNotFoundException;

/**
 * Created by wafs on 24/09/15.
 */
public class Bootstrap {
    public static Router getRouter() throws FileNotFoundException{
        return new Router(Router.loadRoutes(ServerConstants.absPath + "/src/App/Configuration/routes.json"));
    }
}
