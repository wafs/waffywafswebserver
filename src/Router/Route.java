package Router;

/**
 * Created by wafs on 23/09/15.
 */
public class Route {
    protected String name;
    protected String path;
    protected String httpMethod;
    protected String controller;
    protected String function;

    public Route(String name, String path, String httpMethod,  String controller){
        this.name = name;
        this.path = path;
        this.httpMethod = httpMethod;
        String controllerMethod[] = controller.split("@");
        this.controller = controllerMethod[0];
        this.function = controllerMethod[1];
    }

    public Route(String name, String path, String httpMethod,  String controller, String function){
        this.name = name;
        this.path = path;
        this.httpMethod = httpMethod;
        this.controller = controller;
        this.function = function;
    }


    public String getName() {
        return name;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public String getController() {
        return controller;
    }

    public String getFunction() {
        return function;
    }
}
