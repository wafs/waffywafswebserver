package Router.Exceptions;

/**
 * Created by wafs on 24/09/15.
 */
public class RouteNotFoundException extends Exception {
    public RouteNotFoundException(String message){
        super(message);
    }
}
