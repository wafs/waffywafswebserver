package Router.Exceptions;

/**
 * Created by wafs on 24/09/15.
 */
public class MethodNotFoundException extends Exception {

    public MethodNotFoundException(String message){
        super(message);
    }
}
