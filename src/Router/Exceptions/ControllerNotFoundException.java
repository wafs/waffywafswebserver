package Router.Exceptions;

/**
 * Created by wafs on 24/09/15.
 */
public class ControllerNotFoundException extends Exception {
    public ControllerNotFoundException(String message){
        super(message);
    }
}
