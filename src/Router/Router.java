package Router;

import App.Controllers.Controller;
import HTTP.AbstractResponse;
import HTTP.Request;
import Router.Exceptions.ControllerNotFoundException;
import Router.Exceptions.MethodNotFoundException;
import Router.Exceptions.RouteNotFoundException;
import com.oracle.javafx.jmx.json.JSONDocument;
import com.oracle.javafx.jmx.json.JSONReader;
import com.oracle.javafx.jmx.json.impl.JSONStreamReaderImpl;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by wafs on 23/09/15.
 */
public class Router {

    HashMap<String, Route> routes;

    public Router(HashMap<String, Route> routes) {
        this.routes = routes;
    }

    private Route fetchRoute(String name) {
        return routes.get(name);
    }

    /*
     * Loads routes dynamically so the server doesn't need to be restarted
     * when the route file gets updated
     */
    public static HashMap<String, Route> loadRoutes(String path) throws FileNotFoundException {
        // TODO: Add a file watcher to only read routes if a change detected
        JSONReader jr = new JSONStreamReaderImpl(new FileReader(path));
        JSONDocument json = jr.build();
        HashMap<String, Route> routeList = new HashMap<>();

        for (JSONDocument j : json) {
            routeList.put(j.getString("path"), new Route(
                j.getString("name"), j.getString("path"), j.getString("method"), j.getString("controller")
            ));
        }

        return routeList;


    }

    public AbstractResponse invokeRoute(String name, Request request) throws Exception {
        Route route = fetchRoute(name);
        if (route == null) {
            throw new RouteNotFoundException("The requested route is not registered");
        }
        String controller = route.getController();
        if (controller == null) {
            throw new ControllerNotFoundException("Controller File not found at location");
        }

        String function = route.getFunction();
        if (function == null) {
            throw new MethodNotFoundException("Function not found on Controller class");
        }

        /*
            Load & execute the controller function
            via reflection.
         */
        Class<?> c = Class.forName("App.Controllers." + controller);
        Constructor constructor = c.getConstructor(Request.class);
        Controller controllerObject = (Controller) constructor.newInstance(request);
        Method method = c.getDeclaredMethod(function);
        return (AbstractResponse) method.invoke(controllerObject);
    }


}
