package HTTP;

/**
 * Created by wafs on 24/09/15.
 */
public class BadResponse extends AbstractResponse {
    public BadResponse(String body){
        this.body = body;
        this.statusCode = 404;
        this.message = "Not found";
    }
}
