package HTTP;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by wafs on 24/09/15.
 */
public class AbstractResponse {

    protected String httpVersion = "HTTP/1.1";
    protected int statusCode = 200;
    protected String message = "OK";
    protected DateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z");
    protected Date date = new Date();
    protected String contentType = "text/html";
    protected String body = "";

    public void setBody(String body){
        this.body = body;
    }

    public boolean sendResponse(PrintWriter pw){
        StringBuilder sb = new StringBuilder();
        sb.append(httpVersion);
        sb.append(" ");
        sb.append(statusCode);
        sb.append(" ");
        sb.append(message);
        sb.append(System.lineSeparator());
        sb.append("Date: ");
        sb.append(dateFormat.format(date));
        sb.append(System.lineSeparator());
        sb.append("Content-Type: ");
        sb.append(contentType);
        sb.append(System.lineSeparator());
        sb.append("Content-Length: ");
        sb.append(body.length());
        sb.append(System.lineSeparator());
        sb.append(System.lineSeparator());
        sb.append(body);


        String string = sb.toString();
        BufferedReader br  = new BufferedReader(new StringReader(string));
        String next;

        try{
            while((next = br.readLine()) != null){
                pw.println(next);
            }

            return true;
        }catch (Exception e){
            return false;
        }
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

}
