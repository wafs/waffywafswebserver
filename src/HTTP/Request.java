package HTTP;

import java.util.HashMap;
import java.util.Scanner;



// TODO : Process params for POST requests

public class Request {
    private String header = "";
    private String body = "";
    private String requestType = "";
    private String rawRequest = "";
    private String path = "";
    private String rawQuery = "";

    private HashMap<String,String> getParams = new HashMap<>();
    private HashMap<String,String> postParams = new HashMap<>();

    public Request(String rawRequest){
        this.rawRequest = rawRequest;
        splitRawRequest(rawRequest);
        processHeader();
    }

    public void splitRawRequest(String request){
        Scanner scanner = new Scanner(request);
        StringBuilder sb = new StringBuilder();
        String nextLine;

        // Get header
        while(scanner.hasNext()){
            nextLine = scanner.nextLine();
            if(nextLine.equals("")){
                break;
            }
            sb.append(nextLine);
            sb.append(System.lineSeparator());
        }

        header = sb.toString();
        sb.setLength(0);

        while(scanner.hasNext()){
            sb.append(scanner.nextLine());
        }
        body = sb.toString();
        sb.setLength(0);
    }

    public void processHeader(){
        Scanner scanner = new Scanner(header);
        String line;
        while (scanner.hasNext()){
            line = scanner.nextLine();
            String[] tokens = line.split("\\s+");
            switch (tokens[0]){
                case "GET":
                case "POST":
                case "PUT":
                case "PATCH":
                case "DELETE":
                    requestType = tokens[0];
                    rawQuery = tokens[1];
            }
        }

        if(!rawQuery.equals("")){
            String[] splitQuery = rawQuery.split("\\?");
            path = splitQuery[0];

            if(splitQuery.length > 1){
                String[] params = splitQuery[1].split("&");

                for(String s : params){
                    String[] kv = s.split("=");
                    // In case of an empty value, the key should still be maintained
                    if(kv.length == 1){
                        getParams.put(kv[0], "");
                    }else {
                        getParams.put(kv[0], kv[1]);
                    }
                }
            }
        }
    }

    public String getHeader(){
        return header;
    }

    public String getBody(){
        return body;
    }


    public String getPath() {
        return path;
    }

    public HashMap<String, String> getGetParams() {
        return getParams;
    }
}