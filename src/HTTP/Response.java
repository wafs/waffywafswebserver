package HTTP;

public class Response extends AbstractResponse {

    public Response(){}

    public Response(int statusCode, String message, String body){
        this.statusCode = statusCode;
        this.message = message;
        this.body = body;
    }

    public Response(String body){
        this.body = body;
    }
}
