package Server;

import App.Kernel;

import java.net.ServerSocket;

public class Main {
    public static void main(String[] args) {

        /*
         * Set up paths to be used by the entire app
         */
        try {
            ServerConstants.loadPaths(ServerConstants.absPath + "/src/App/Configuration/paths.json");
        }catch (Exception e){
            e.printStackTrace();
        }


        System.out.println("Starting Server on Port 1331");
        try(ServerSocket serverSocket = new ServerSocket(1331)){
            while(true){
                Kernel t = new Kernel(serverSocket.accept());
                System.out.println("Client Connected");
                t.run();
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
