package Server;


import Router.Route;
import com.oracle.javafx.jmx.json.JSONDocument;
import com.oracle.javafx.jmx.json.JSONReader;
import com.oracle.javafx.jmx.json.impl.JSONStreamReaderImpl;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * This class should statically hold constants required by the app to run.
 * Set the constants as JSON key=value pairs in the default config directory.
 * You may reference a previously named path by using it in a pattern like "%app%".
 * This will replace "%app%" in your path string with app's path.
 *
 */
public class ServerConstants {

    public final static String absPath = System.getProperty("user.dir");
    private static Map<String, String> paths;

    public static void loadPaths(String filePath) throws IOException {
        JSONReader jr = new JSONStreamReaderImpl(new FileReader(filePath));
        JSONDocument json = jr.build();
        HashMap<String, String> paths = new HashMap<>();

        Pattern pattern = Pattern.compile("%([^%]*)%");
        Matcher matcher;

        for (String key : json.object().keySet()) {
            String path = (String) json.object().get(key);
            matcher = pattern.matcher(path);
            if (matcher.find()) {
                String replacement = paths.get(matcher.group(1));
                path = matcher.replaceAll(replacement);
            }
            paths.put(key, path);
        }

        ServerConstants.paths = paths;
    }

    public static String getPath(String name) {
        return absPath + paths.get(name);
    }
}
